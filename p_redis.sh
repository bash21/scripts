#!/bin/bash

n_name='nora'
w_name='wbs'
p_id='m.peshko'

function gen {
  for i in $p_id;
    do
      g_passwd=`pwgen -s 16 | awk '{print $1}'`
      echo -e "user $n_name"_"$i on >$g_passwd ~*$i:$n_name:* +@all \nuser $w_name"_"$i on >$g_passwd ~*$i:$w_name:* +@all"
    done
}

function main {
  gen >> p_redis.txt
}

main
