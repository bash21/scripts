#!/bin/bash

s_dir=/backup/local/bash/
d_dir=/tmp/$(date +%Y-%m-%d:%k:%M:%S)-1.tar

for i in $s_dir*; do
  tar -rv -f $d_dir -C $s_dir ${i##*/}
done
