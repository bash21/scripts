#!/bin/bash

echo -n "Enter dir: "
read sdir

if [ -n "$sdir" ]
then
    for file in "$sdir"
    do
        echo -e "\tStart folder $file"
        for i in $file/*
        do
            if [ -d "$i" ]
            then
                echo "folder: $i"
            elif [ -f "$i" ]
            then
                echo "file: $i"
            elif [ -x "$i" ]
            then
                echo "executable file $i"
            fi
        done
    done

else
    for file in "$PWD"
    do
        echo -e "\tStart folder $file"
        for i in $file/*
        do
            if [ -d "$i" ]
            then
                echo "folder: $i"
            elif [ -f "$i" ]
            then
                echo "file: $i"
            elif [ -x "$i" ]
            then
                echo "executable file $i"
            fi
        done
    done
fi
