#!/bin/bash

set -eu

KEYSTORE_FILENAME="keystore.jks"
VALIDITY_IN_DAYS=3650
DEFAULT_TRUSTSTORE_FILENAME="truststore.jks"
TRUSTSTORE_WORKING_DIRECTORY="truststore"
KEYSTORE_WORKING_DIRECTORY="keystore"
CA_CERT_FILE="ca-cert"
KEYSTORE_SIGN_REQUEST="key-unsign"
KEYSTORE_SIGN_REQUEST_SRL="ca-cert.srl"
KEYSTORE_SIGNED_CERT="key-signed"

trust_store_file="$TRUSTSTORE_WORKING_DIRECTORY/$DEFAULT_TRUSTSTORE_FILENAME"

COUNTRY=
STATE=
OU=
CN=
LOCATION=
PASS=


function gen_ca {
    CN="hostname"
    TRUSTSTORE_WORKING_DIRECTORY="truststore"

    openssl req -new -x509 -keyout $TRUSTSTORE_WORKING_DIRECTORY/ca-key \
    -out $TRUSTSTORE_WORKING_DIRECTORY/ca-cert -days $VALIDITY_IN_DAYS -nodes \
    -subj "/C=$COUNTRY/ST=$STATE/L=$LOCATION/O=$OU/CN=$CN"
}

function trust_store {
    TRUSTSTORE_WORKING_DIRECTORY="truststore"
    DEFAULT_TRUSTSTORE_FILENAME="truststore.jks"
    CN="hostname"
    CA_CERT_FILE="ca-cert"
    PASS=

    keytool -keystore $TRUSTSTORE_WORKING_DIRECTORY/$DEFAULT_TRUSTSTORE_FILENAME \
    -alias CARoot -import -file $TRUSTSTORE_WORKING_DIRECTORY/ca-cert \
    -noprompt -dname "C=$COUNTRY, ST=$STATE, L=$LOCATION, O=$OU, CN=$CN" -keypass $PASS -storepass $PASS
}

function key_store {
    KEYSTORE_WORKING_DIRECTORY="keystore"
    KEYSTORE_FILENAME="keystore.jks"
    CN="hostname"
    PASS=

    keytool -keystore $KEYSTORE_WORKING_DIRECTORY/$KEYSTORE_FILENAME \
    -alias localhost -validity $VALIDITY_IN_DAYS -genkey -keyalg RSA \
    -noprompt -dname "CN=$CN" -keypass $PASS -storepass $PASS
}

function get_cert-key-store {
    KEYSTORE_WORKING_DIRECTORY="keystore"
    KEYSTORE_FILENAME="keystore.jks"
    KEYSTORE_SIGN_REQUEST="key-unsign"
    PASS=

    keytool -keystore $KEYSTORE_WORKING_DIRECTORY/$KEYSTORE_FILENAME -alias localhost \
    -certreq -file $KEYSTORE_WORKING_DIRECTORY/$KEYSTORE_SIGN_REQUEST -keypass $PASS -storepass $PASS
}

function sign_cert-key-store {
    TRUSTSTORE_WORKING_DIRECTORY="truststore"
    trust_store_private_key_file="$TRUSTSTORE_WORKING_DIRECTORY/ca-key"
    CA_CERT_FILE="ca-cert"
    KEYSTORE_SIGN_REQUEST="key-unsign"
    KEYSTORE_SIGNED_CERT="key-sign"

    openssl x509 -req -CA $TRUSTSTORE_WORKING_DIRECTORY/$CA_CERT_FILE -CAkey $trust_store_private_key_file \
    -in $KEYSTORE_WORKING_DIRECTORY/$KEYSTORE_SIGN_REQUEST -out $KEYSTORE_WORKING_DIRECTORY/$KEYSTORE_SIGNED_CERT \
    -days $VALIDITY_IN_DAYS -CAcreateserial
}

function iport_ca {
    KEYSTORE_WORKING_DIRECTORY="keystore"
    KEYSTORE_FILENAME="keystore.jks"
    CA_CERT_FILE="ca-cert"

    keytool -keystore $KEYSTORE_WORKING_DIRECTORY/$KEYSTORE_FILENAME -alias CARoot \
    -import -file $TRUSTSTORE_WORKING_DIRECTORY/$CA_CERT_FILE -keypass $PASS -storepass $PASS -noprompt
}

function iport_key {
    KEYSTORE_WORKING_DIRECTORY="keystore"
    KEYSTORE_FILENAME="keystore.jks"
    KEYSTORE_SIGNED_CERT="key-sign"

    keytool -keystore $KEYSTORE_WORKING_DIRECTORY/$KEYSTORE_FILENAME -alias localhost -import \
    -file $KEYSTORE_WORKING_DIRECTORY/$KEYSTORE_SIGNED_CERT -keypass $PASS -storepass $PASS
}

function clen {
    rm $KEYSTORE_SIGN_REQUEST_SRL
    rm $KEYSTORE_SIGN_REQUEST
    rm $KEYSTORE_SIGNED_CERT
}

function get_ca-cert {
    TRUSTSTORE_WORKING_DIRECTORY="truststore"
    DEFAULT_TRUSTSTORE_FILENAME="kafka.truststore.jks"
    trust_store_file="$TRUSTSTORE_WORKING_DIRECTORY/$DEFAULT_TRUSTSTORE_FILENAME"

    keytool -keystore $trust_store_file -export -alias CARoot -rfc -file $CA_CERT_FILE -keypass $PASS -storepass $PASS
}

function main {
#    gen_ca
#    trust_store
    echo ""
}

function client {
    trust_store
    key_store
    get_cert-key-store
    sign_cert-key-store
    iport_ca
    iport_key
#    get_ca-cert
}

function dell {
    clen
}

client
