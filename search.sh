#!/bin/bash

read -p "Enter folder: " folder
read -p "Enter search word: " word

function search {
  grep -rwl "$folder" -e "$word"
  grep -rwl "$folder" -e "$word" | echo "Find: $(wc -l)"
}

function main {
  search
}

main
