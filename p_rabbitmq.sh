#!/bin/bash

#######
###c_name = container c_name
###u_name = the name of the user to be created
###v_name = virtual host name
###p_id = PID provider
###o_file = output file contains passwords/deleted user
###error.txt = output errors
#######
c_name=' '
u_name=' '
v_name=' '
p_id=' '
o_file=$(date +%Y-%m-%d:%k:%M:%S)-1.txt

function n_gen {
#######
###Created user nora_PID and wbs_PID with passwords. Output file save information about passwords.
#######
  for i in $p_id;
    do
      for l in $u_name;
        do
          g_passwd=`pwgen -s 64 | awk '{print $1}'`
          docker exec $c_name bash -c "rabbitmqctl add_user $l"_"$i $g_passwd" 1>/dev/null 2>>./error.txt
          docker exec $c_name bash -c "rabbitmqctl set_permissions -p $v_name $l"_"$i \".*\" \".*\" \".*\"" 1>/dev/null 2>>./error.txt
          echo "rabbitmqctl add_user $l"_"$i $g_passwd "
      done
    done
}

function d_user {
  #######
  ###Delet user nora_PID and wbs_PID with passwords. Output file save information about deleted users.
  #######
  for i in $p_id;
    do
      for l in $u_name;
        do
        docker exec $c_name bash -c "rabbitmqctl delete_user $l"_"$i"
      done
    done
}

function main {
#  n_gen
  d_user
}

main >> $o_file
