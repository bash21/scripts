#!/bin/bash

packages=('openvswitch-switch')


for package in "${packages[@]}"; do
    echo "Starting install openvswitch_install. WAIT..."
    apt install -y "$package" &>/dev/null
    done

if [[ "$?" -eq "0" ]]; then
    systemctl enable --now ${packages[0]}
    echo "${packages[0]} is installed and started"
else
    exit 1
fi