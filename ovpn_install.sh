#!/bin/bash

#######SERVER

#echo "deb http://build.openvpn.net/debian/openvpn/release/2.5 bionic main" > /etc/apt/sources.list.d/openvpn-aptrepo.list
#1)Install OpenVPN and EASY-RSA
#    sudo apt -y install openvpn easy-rsa

#2)Copy scripts
#    cp -R /usr/share/easy-rsa /etc/openvpn/

#3)Initialize the PKI
#    cd /etc/openvpn/easy-rsa
#    cp openssl-1.0.0.cnf openssl.cnf
#    . ./vars
#    ./clean-all
#    ./build-ca
#4)Generate certificate & key for server
#    cd /etc/openvpn/easy-rsa
#    ./build-key-server server.spnode.net
#5)Generate Diffie Hellman parameters
#    cd /etc/openvpn/easy-rsa
#    ./build-dh

#6)Create config file
#sudo cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz /etc/openvpn/server/
#sudo gunzip /etc/openvpn/server/server.conf.gz

#7)Config server
#    echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
#    sysctl -p

#8)Start OpenVPN
#    sudo systemctl -f enable openvpn-server@server.service
#    sudo systemctl start openvpn-server@server.service

#######CLIENT
#1)Copy config
#    cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf /etc/openvpn/client/client.conf

#)Generate certificates & keys for clients
#    cd /etc/openvpn/easy-rsa
#    ./build-key client1.spnode.net
